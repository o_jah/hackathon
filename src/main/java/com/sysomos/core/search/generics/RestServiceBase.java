package com.sysomos.core.search.generics;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Provides base functionality for sending post requests to a an external web
 * service.
 * 
 * <p>
 * Usage:<br>
 * ~~~~~<br>
 * Define the <code>baseurl</code> and the endpoint or <code>resource</code> in
 * the constructor.<br>
 * Create all of the POST parameters as a MultivaluedMap, and call
 * <code>post(MultivaluedMap)</code> or
 * <code>postAsString(MultivaluedMap)</code>
 * 
 * @author JasonLiu
 * 
 * @see MultivaluedMap
 * @see ClientResponse
 *
 */
public class RestServiceBase {

	private static final Logger LOG = LoggerFactory.getLogger("RestService");

	private static String REST_URL;

	private static String RESOURCE;

	private final Client client;

	public RestServiceBase(String baseurl, String resource) {
		REST_URL = baseurl;
		RESOURCE = resource;
		client = Client.create();
	}

	public ClientResponse post(MultivaluedMap<String, String> params) {
		WebResource webResource = client.resource(REST_URL + RESOURCE);
		ClientResponse response = webResource.queryParams(params)
				.type(MediaType.APPLICATION_JSON).post(ClientResponse.class);
		if (response.getStatus() != 200) {
			getLog().error("Grid: for request: {} Invalid response received {}", webResource, response);
		}
		return response;
	}

	public static Logger getLog() {
		return LOG;
	}

}