package com.sysomos.core.search.transfer;

/**
 * 
 * @author JasonLiu
 *
 */
public class ProfileVO {

	private String profileUrl;
	private long created;
	private long avgPostPerDay;
	private String timezone;
	private String description;
	private String id;
	private String name;
	private long followerCount;
	private String country;
	private String language;
	private long postCount;
	private String pictureUrl;
	private long lastPostDate;
	private String dataSource;
	private String screenName;
	private long friendCount;
	private String city;
	private long listedCount;
	private String province;
	private long influenceScore;
	private String verified;
	private long likedCount;
	private String gender;
	private String rawLocation;

	public String getProfileUrl() {
		return profileUrl;
	}

	public long getCreated() {
		return created;
	}

	public long getAvgPostPerDay() {
		return avgPostPerDay;
	}

	public String getTimezone() {
		return timezone;
	}

	public String getDescription() {
		return description;
	}

	public long getId() {
		return trimTT(id);
	}

	public String getName() {
		return name;
	}

	public long getFollowerCount() {
		return followerCount;
	}

	public String getCountry() {
		return country;
	}

	public String getLanguage() {
		return language;
	}

	public long getPostCount() {
		return postCount;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public long getLastPostDate() {
		return lastPostDate;
	}

	public String getDataSource() {
		return dataSource;
	}

	public String getScreenName() {
		return screenName;
	}

	public long getFriendCount() {
		return friendCount;
	}

	public String getCity() {
		return city;
	}

	public long getListedCount() {
		return listedCount;
	}

	public String getProvince() {
		return province;
	}

	public long getInfluenceScore() {
		return influenceScore;
	}

	public String getVerified() {
		return verified;
	}

	public long getLikedCount() {
		return likedCount;
	}

	public String getGender() {
		return gender;
	}

	public String getRawLocation() {
		return rawLocation;
	}

	private static final Long trimTT(String id) {
		if (id.contains("TT")) {
			return Long.valueOf(id.substring(2));
		} else {
			return Long.valueOf(id);
		}
	}
}
