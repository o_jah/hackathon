package com.sysomos.core.search.transfer;

/**
 * 
 * @author JasonLiu
 *
 */
public enum Relationship {
	FRIENDS, FOLLOWERS, INDUCED;
}
