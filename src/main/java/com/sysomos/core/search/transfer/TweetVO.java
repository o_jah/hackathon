package com.sysomos.core.search.transfer;

/**
 * 
 * @author JasonLiu
 *
 */
public class TweetVO {

	private long docId;
	private String actorName;
	private String actorScreenName;
	private String actorId;
	private String contents;

	private String permaLink;
	private long createDate;
	private String lang;

	private String refDocId;
	private String refUserId;

	private String postSource;

	private int influenceScore;
	private long profanityScore;

	private String city;
	private String country;

	public long getDocId() {
		return docId;
	}

	public long getActorId() {
		return trimTT(actorId);
	}

	public String getActorName() {
		return actorName;
	}

	public String getActorScreenName() {
		return actorScreenName;
	}

	public String getContents() {
		return contents;
	}

	// Setting is allowed in case for tweet cleaning
	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getPermaLink() {
		return permaLink;
	}

	public long getCreateDate() {
		return createDate;
	}

	public String getLang() {
		return lang;
	}

	public String getRefDocId() {
		return refDocId;
	}

	public void setRefDocId(String refDocId) {
		this.refDocId = refDocId;
	}

	public String getRedUserId() {
		return refUserId;
	}

	public String getPostSource() {
		return postSource;
	}

	public int getInfluenceScore() {
		return influenceScore;
	}

	public long getProfanityScore() {
		return profanityScore;
	}

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	private static final Long trimTT(String id) {
		if (id.contains("TT")) {
			return Long.valueOf(id.substring(2));
		} else {
			return Long.valueOf(id);
		}
	}
}