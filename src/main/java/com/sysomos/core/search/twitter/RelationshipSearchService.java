package com.sysomos.core.search.twitter;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import com.sysomos.core.search.transfer.Relationship;

/**
 * 
 * @author JasonLiu
 * 
 * @see Relationship
 *
 */
public interface RelationshipSearchService {

	/**
	 * Obtains the desired ids from a single userid <code>id</code> specified by
	 * <code>relationship</code>
	 * 
	 * @param relationship
	 * @param id
	 * @return
	 * @throws IOException
	 */
	Collection<Long> get(Relationship relationship, Long id) throws IOException;

	/**
	 * Obtains the desired ids to <code>id</code> map specified by
	 * <code>relationship</code>
	 * 
	 * @param relationship
	 * @param ids
	 * @return
	 * @throws IOException
	 */
	Map<Long, Collection<Long>> get(Relationship relationship, Collection<Long> ids) throws IOException;

}
