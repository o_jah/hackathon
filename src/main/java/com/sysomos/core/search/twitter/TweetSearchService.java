package com.sysomos.core.search.twitter;

import com.sysomos.core.search.transfer.SearchResult;
import com.sysomos.core.search.transfer.TweetVO;
import com.sysomos.hackathon.solr.SOLRTweet;

/**
 * 
 * @author JasonLiu
 * 
 * @see TweetVO
 *
 */
public interface TweetSearchService {

	/**
	 * Query for the most recent tweets. The defaults are the following
	 *
	 * <p>
	 * "actorName", "actorScreenName", "docId", "contents", "actorId",
	 * "createDate"
	 * 
	 * @param query
	 * @param startDateMillis
	 * @param endDateMillis
	 * @param maxResults
	 * @return
	 */
	SearchResult<SOLRTweet> search(String query, long startDateMillis,
			long endDateMillis, int maxResults);

	/**
	 * Query for tweets randomly spread over <code>startDateMillis</code> and
	 * <code>endDateMillis</code>
	 * 
	 * @param query
	 * @param startDateMillis
	 * @param endDateMillis
	 * @param maxResults
	 * @return
	 */
	SearchResult<SOLRTweet> searchRandom(String query, long startDateMillis,
			long endDateMillis, int maxResults);

	/**
	 * Query for the retweets of a certain tweet randomly spread over
	 * <code>startDateMillis</code> and <code>endDateMillis</code>
	 * 
	 * @param tweetId
	 * @param startDateMillis
	 * @param endDateMillis
	 * @param maxResults
	 * @return
	 */
	SearchResult<SOLRTweet> searchByRetweet(long tweetId, long startDateMillis,
			long endDateMillis, int maxResults);

	/**
	 * Query for the most recent tweets.
	 * 
	 * @param author
	 * @param startDateMillis
	 * @param endDateMillis
	 * @param maxResults
	 * @return
	 */
	SearchResult<SOLRTweet> searchByAuthor(long author, long startDateMillis,
			long endDateMillis, int maxResults);

}
