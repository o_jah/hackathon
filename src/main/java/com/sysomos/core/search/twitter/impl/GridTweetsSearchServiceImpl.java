package com.sysomos.core.search.twitter.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sysomos.core.search.config.DataAccessConfig;
import com.sysomos.core.search.generics.RestServiceBase;
import com.sysomos.core.search.transfer.SearchResult;
import com.sysomos.core.search.transfer.TweetVO;
import com.sysomos.core.search.twitter.TweetSearchService;
import com.sysomos.hackathon.solr.SOLRTweet;

/**
 * Provides access to GRID's solr index (over REST) for twitter tweet data.
 * 
 * @author JasonLiu
 * 
 * @see TweetVO
 */
public class GridTweetsSearchServiceImpl extends RestServiceBase implements
		TweetSearchService {

	private static final String RESOURCE = DataAccessConfig.SOLR_POST_RESOURCE
			.toString();
	private static final String BASE_URL = DataAccessConfig.SOLR_BASE_URL_PRODUCT
			.toString(); // SOLR_BASE_URL_STAGING

	private static ObjectMapper objectMapper;
	private static SimpleModule deserializer;

	private static final String[] DEFAULT_FIELDS = { "actorName",
			"actorScreenName", "docId", "contents", "actorId", "createDate",
			"hashTags" };

	static {
		deserializer = new SimpleModule();
		objectMapper = new ObjectMapper();
		deserializer.setMixInAnnotation(SOLRTweet.class, TweetMixin.class);
		objectMapper.registerModule(deserializer);
	}

	public GridTweetsSearchServiceImpl() {
		super(BASE_URL, RESOURCE);
	}

	private MultivaluedMap<String, String> constructParams(
			long startDateMillis, long endDateMillis, int maxResults,
			List<String> desiredFields) {
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();
		String fields = StringUtils.join(desiredFields, "|");
		params.putSingle("dataSrcs", "TT");
		params.putSingle("rows", String.valueOf(maxResults));
		params.putSingle("startDate", String.valueOf(startDateMillis));
		params.putSingle("endDate", String.valueOf(endDateMillis));
		params.putSingle("startRow", String.valueOf(0));
		params.putSingle("fields", fields);
		return params;
	}

	private SearchResult<SOLRTweet> getResults(ClientResponse response) {
		GridAPIResponse apiResponse = new GridAPIResponse();
		try {
			apiResponse = objectMapper.readValue(
					response.getEntityInputStream(), GridAPIResponse.class);
		} catch (IOException e) {
			getLog().error(
					"Error parsing Grid twitter response for response: "
							+ response, e);
			e.printStackTrace();
		}
		return new SearchResult<SOLRTweet>("TT", apiResponse.results,
				apiResponse.recordFound);
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	private static class GridAPIResponse {
		public int statusCode;
		public String errorMessage;
		public long recordFound;
		public int recordReturned;
		public List<SOLRTweet> results;

		@Override
		public String toString() {
			return "GridAPIResponse{" + "statusCode=" + statusCode
					+ ", errorMessage='" + errorMessage + '\''
					+ ", recordFound=" + recordFound + ", recordReturned="
					+ recordReturned + '}';
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	private abstract class TweetMixin {

		@JsonProperty(value = "docId")
		public long docId;
		@JsonProperty(value = "actorName")
		public String actorName;
		@JsonProperty(value = "actorScreenName")
		public String actorScreenName;
		@JsonProperty(value = "actorId")
		public String actorId;
		@JsonProperty(value = "contents")
		public String contents;

		@JsonProperty(value = "permaLink")
		public String permaLink;
		@JsonProperty(value = "createDate")
		public long createDate;
		@JsonProperty(value = "lang")
		public String lang;

		@JsonProperty(value = "refDocId")
		public String refDocId;
		@JsonProperty(value = "refUserId")
		public String refUserId;

		@JsonProperty(value = "postSource")
		public String postSource;

		@JsonProperty(value = "influenceScore")
		public int influenceScore;
		@JsonProperty(value = "profanityScore")
		public long profanityScore;

		@JsonProperty(value = "city")
		public String city;
		@JsonProperty(value = "province")
		public String province;
		@JsonProperty(value = "country")
		public String country;
	}

	public SearchResult<SOLRTweet> search(String query, long startDateMillis,
			long endDateMillis, int maxResults) {
		MultivaluedMap<String, String> params = constructParams(
				startDateMillis, endDateMillis, maxResults,
				Arrays.asList(DEFAULT_FIELDS));
		params.putSingle("query", query);
		return getResults(post(params));
	}

	public SearchResult<SOLRTweet> searchRandom(String query,
			long startDateMillis, long endDateMillis, int maxResults) {
		MultivaluedMap<String, String> params = constructParams(
				startDateMillis, endDateMillis, maxResults,
				Arrays.asList(DEFAULT_FIELDS));
		params.putSingle("query", query);
		params.putSingle("sortBy", "random");
		return getResults(post(params));
	}

	public SearchResult<SOLRTweet> searchByRetweet(long tweetId,
			long startDateMillis, long endDateMillis, int maxResults) {
		MultivaluedMap<String, String> params = constructParams(
				startDateMillis, endDateMillis, maxResults,
				Arrays.asList(DEFAULT_FIELDS));
		params.putSingle("fq", "refDocId:TT" + String.valueOf(tweetId));
		params.putSingle("sortBy", "random");
		return getResults(post(params));
	}

	public SearchResult<SOLRTweet> searchByAuthor(long author,
			long startDateMillis, long endDateMillis, int maxResults) {
		MultivaluedMap<String, String> params = constructParams(
				startDateMillis, endDateMillis, maxResults,
				Arrays.asList(DEFAULT_FIELDS));
		params.putSingle("fq", "actorId:TT" + String.valueOf(author));
		return getResults(post(params));
	}
}