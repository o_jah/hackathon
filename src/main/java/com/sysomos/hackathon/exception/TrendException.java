package com.sysomos.hackathon.exception;

public class TrendException extends Exception {

	private static final long serialVersionUID = 3978457093666720154L;

	public TrendException(String message) {
		super(message);
	}
}
