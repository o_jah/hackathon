package com.sysomos.hackathon.trend;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import com.beust.jcommander.Parameter;
import com.sysomos.hackathon.exception.TrendException;
import com.sysomos.hackathon.solr.QuerySOLR;
import com.sysomos.hackathon.solr.SOLRTweet;
import com.sysomos.hackathon.utils.Counter;
import com.sysomos.hackathon.utils.Pair;

public class ContextTrend {

	private enum Stats {
		MEAN, STDEV, VELOCITY, ACCELERATION;
	}

	public static class Parameters {
		@Parameter(names = { "-query" }, required = true, description = "Specifies a query or a list "
				+ "of queries", variableArity = true)
		public String query;
		@Parameter(names = "-startDateMillis", required = true, description = "Start Date in milliseconds")
		public Long startDateMillis = Long.MIN_VALUE;
		@Parameter(names = "-endDateMillis", required = true, description = "End Date in milliseconds")
		public Long endDateMillis = Long.MIN_VALUE;
		@Parameter(names = "-top", required = false, description = "Top most occurring documents")
		public int top = 20;
		@Parameter(names = "-baseline", required = false, description = "Num hours to consider when computing mean and stdev")
		public int baseline = 6;
		@Parameter(names = "-hour", required = false, description = "Input hour")
		public int hour;

	}

	/**
	 * @param top
	 *            Number to consider when outputting the topmost hashtags.
	 * @param hastagsPerHour
	 *            Counter for hashTags on an hourly basis.
	 * @return Top most recurrent hashtags
	 */
	@SuppressWarnings("unchecked")
	private Counter<String> getHashtagCounter(
			final Counter<Pair<String, Integer>> hastagsPerHour) {
		if (hastagsPerHour == null) {
			return null;
		}
		Counter<String> hashtagCounter = new Counter<String>();
		for (Object key : hastagsPerHour.keys()) {
			Pair<String, Integer> pair = (Pair<String, Integer>) key;
			hashtagCounter.add(pair.getA(), hastagsPerHour.getCount(pair));
		}
		return hashtagCounter;
	}

	/**
	 * @param results
	 *            List of tweets matching a certain search keyword.
	 * @return A counter for hashtags
	 */
	private Counter<Pair<String, Integer>> hashTagCount(
			final List<SOLRTweet> results, int currentHour) {

		if (CollectionUtils.isEmpty(results)) {
			System.out.println("SOLR Tweet Results is null.");
			return null;
		}

		Counter<Pair<String, Integer>> hashtagsPerHour;
		hashtagsPerHour = new Counter<Pair<String, Integer>>();

		for (SOLRTweet tweet : results) {
			DateTime createDate = new DateTime(tweet.getCreateDate());
			int hour = createDate.getHourOfDay();
			if (hour > currentHour) {
				continue;
			}
			String[] hashTags = tweet.getHashTags();
			if (hashTags == null) {
				continue;
			}
			for (String hashtag : hashTags) {
				hashtagsPerHour.add(new Pair<String, Integer>(hashtag
						.toLowerCase(), hour));
			}
		}
		return hashtagsPerHour;
	}

	/**
	 * @param hTagCount
	 *            Counter for hashtags
	 * @param topHashtags
	 *            List of most occurring hashtags
	 * @param window
	 *            Size of the time window
	 * @param ub
	 *            the end time for the time window to consider when querying
	 *            SOLR.
	 * @return a Map of statistics with their corresponding values.
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Map<Stats, Double>> statistics(
			final Counter<Pair<String, Integer>> hTagCount,
			final List<String> topHashtags, int window, int end) {

		int start = (1 + end - window) <= 0 ? (24 + (1 + end - window)) : 1
				+ end - window;
		end = (end < start) ? start : end;

		Map<String, Map<Stats, Double>> hTagStats = new HashMap<String, Map<Stats, Double>>();
		for (String hTag : topHashtags) {
			List<Integer> counts = new ArrayList<Integer>();
			for (Object key : hTagCount.keys()) {
				Pair<String, Integer> pair = (Pair<String, Integer>) key;
				if (pair.getA().equals(hTag) && start <= pair.getB()
						&& pair.getB() <= end) {
					counts.add(hTagCount.getCount(pair));
				}
			}
			Map<Stats, Double> stats = new HashMap<Stats, Double>();
			double meanValue = mean(counts, window);
			stats.put(Stats.MEAN, meanValue);
			stats.put(Stats.VELOCITY, velocity(null));
			stats.put(Stats.STDEV, stdev(counts, meanValue));
			stats.put(Stats.ACCELERATION, acceleration());

			hTagStats.put(hTag, stats);
		}
		return hTagStats;
	}

	/**
	 * @param list
	 *            List of integers.
	 * @return the empirical mean of the list.
	 */
	private double mean(List<Integer> list, int size) {
		if (CollectionUtils.isEmpty(list) || size == 0) {
			return Integer.MIN_VALUE;
		}
		int sum = 0;
		for (Integer element : list) {
			sum += element;
		}
		return (1.0 * sum) / size;
	}

	private double velocity(List<Integer> list) {
		return 0;
	}

	private double stdev(List<Integer> list, double mean) {
		return 0;
	}

	private double acceleration() {
		return 0;
	}

	/**
	 * @param args
	 *            Command Line arguments
	 * @return List of trending topics (or contexts)
	 * @throws TrendException
	 */
	public List<Pair<String, Pair<Double, Integer>>> getTrendingTopics(
			final Parameters args) throws TrendException {
		if (StringUtils.isEmpty(args.query)) {
			throw new TrendException("Query is empty");
		}

		QuerySOLR solr = new QuerySOLR();
		List<SOLRTweet> matchingTweets;

		if (args.startDateMillis > 0 && args.endDateMillis > 0) {
			matchingTweets = solr.getMatchingTweets(args.query,
					args.startDateMillis, args.endDateMillis);
		} else {
			matchingTweets = solr.getMatchingTweets(args.query);
		}

		Counter<Pair<String, Integer>> hTagCountPerHour = hashTagCount(
				matchingTweets, args.hour);
		Counter<String> counterH = getHashtagCounter(hTagCountPerHour);
		List<String> topHashtags = counterH.getTop(args.top);
		prettyPrinting(topHashtags, counterH, args.top);

		Map<String, Map<Stats, Double>> stats = statistics(hTagCountPerHour,
				topHashtags, args.baseline, args.hour - 1);

		List<Pair<String, Pair<Double, Integer>>> trending;
		trending = new ArrayList<Pair<String, Pair<Double, Integer>>>();

		for (String htag : topHashtags) {
			double mean = stats.get(htag).get(Stats.MEAN);
			if (mean <= 0) {
				continue;
			}
			Pair<String, Integer> key = new Pair<String, Integer>(htag,
					args.hour);
			int count = hTagCountPerHour.getCount(key);
			if (count >= Math.floor(2 * mean)) {
				Pair<Double, Integer> hStats = new Pair<Double, Integer>(mean,
						count);
				trending.add(new Pair<String, Pair<Double, Integer>>(htag,
						hStats));
			}
		}

		Comparator<Pair<String, Pair<Double, Integer>>> cmp;
		cmp = new Comparator<Pair<String, Pair<Double, Integer>>>() {
			@Override
			public int compare(Pair<String, Pair<Double, Integer>> o1,
					Pair<String, Pair<Double, Integer>> o2) {
				return o2.getB().getB().compareTo(o1.getB().getB());
			}
		};
		Collections.sort(trending, cmp);

		return trending;
	}

	private void prettyPrinting(List<String> topHashtags,
			final Counter<String> counter, int top) {
		if (CollectionUtils.isEmpty(topHashtags)) {
			return;
		}

		System.out.println("*******************************");
		String hTTags = "";
		for (int i = 0; i < topHashtags.size(); i++) {
			if (i < topHashtags.size() - 1) {
				hTTags += topHashtags.get(i) + "("
						+ counter.getCount(topHashtags.get(i)) + "), ";
				continue;
			}
			hTTags += topHashtags.get(i) + "("
					+ counter.getCount(topHashtags.get(i)) + ")";
		}
		System.out.println("Top " + top + " HashTags: " + hTTags);
	}
}
